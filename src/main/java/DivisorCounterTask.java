/**
 * 
 * @author Stephanie Kaswurm, Anna-Lena Klaus
 *
 */


public class DivisorCounterTask implements Runnable{

	private DivisorCounterTaskResult result;
	private long zahl;
	
	public DivisorCounterTask(DivisorCounterTaskResult result, long zahl){
		this.result = result;
		this.zahl = zahl;
	}
	
	@Override
	public void run() {
		int anzahlDivisoren = 2;
		
		
		for(int candidate = 2; candidate <= zahl / 2; candidate++ ){
			if(zahl % candidate == 0) {
				++ anzahlDivisoren;
			}
		}
		
	result.publishResult(zahl, anzahlDivisoren);
	
	}
}
