
public class DivisorCounterTaskResult {
	
	private long aktuelleZahl;
	private int aktuelleAnzahlDivisoren;
	
	
	public DivisorCounterTaskResult(){
		
	}
	
	public synchronized void publishResult (long zahl, int anzahlDivisoren){
		if(anzahlDivisoren > aktuelleAnzahlDivisoren) {
			aktuelleAnzahlDivisoren = anzahlDivisoren;
			aktuelleZahl = zahl;
		}
		
		else if (anzahlDivisoren == aktuelleAnzahlDivisoren){
			if(zahl < aktuelleZahl){
				aktuelleAnzahlDivisoren = anzahlDivisoren;
				aktuelleZahl = zahl;
			}
		}
	}
	
	
	public long getAktuelleZahl(){
		return aktuelleZahl;
	}
	
	public long getaktuelleAnzahlDivisoren(){
		return aktuelleAnzahlDivisoren;
	}

}
